package org.ecampus.mrc.activemq;

public interface BrokerConfiguration {

  String getBrokerName();
  String getHost();
  int getPort();

}
