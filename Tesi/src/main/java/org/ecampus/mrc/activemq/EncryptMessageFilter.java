package org.ecampus.mrc.activemq;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.management.ObjectName;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFilter;
import org.apache.activemq.broker.Connection;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.broker.ProducerBrokerExchange;
import org.apache.activemq.broker.TransportConnection;
import org.apache.activemq.broker.TransportConnectionState;
import org.apache.activemq.broker.region.Subscription;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.activemq.command.ConnectionId;
import org.apache.activemq.command.ConsumerId;
import org.apache.activemq.command.ConsumerInfo;
import org.apache.activemq.command.Message;
import org.apache.activemq.command.MessageDispatch;
import org.apache.activemq.util.ByteSequence;
import org.apache.commons.lang3.StringUtils;
import org.ecampus.mrc.crypto.symmetric.AES;
import org.ecampus.mrc.crypto.symmetric.SymmetricAlgorithm;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.ecampus.mrc.json.JSONUtils;
import org.ecampus.mrc.model.ClientSession;
import org.ecampus.mrc.model.TopicSession;
import org.ecampus.mrc.model.TopicSessionMessage;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.ecampus.mrc.repository.ClientRegistrationRepository;
import org.ecampus.mrc.repository.TopicSessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class EncryptMessageFilter extends BrokerFilter {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private ClientRegistrationRepository clientRegistrationRepository;
  private TopicSessionRepository topicSessionRepo;

  protected List<String> internalTopic;
  protected List<String> excludeTopic;

  public EncryptMessageFilter(Broker next,
      ClientRegistrationRepository clientRegistrationRepository,
      TopicSessionRepository topicSessionRepo, List<String> excludeTopic,
      List<String> internalTopic) {
    super(next);
    this.excludeTopic = excludeTopic;
    this.internalTopic = internalTopic;
    this.clientRegistrationRepository = clientRegistrationRepository;
    this.topicSessionRepo = topicSessionRepo;
  }

  @Override
  public void send(ProducerBrokerExchange producerExchange, Message messageSend) throws Exception {
    String topic = messageSend.getDestination().getPhysicalName();
    System.out.println("Broker Receive Message "+ new SimpleDateFormat("dd-MM-yy:HH:mm:SS").format(new Date()));
    if(!MQTTUtils.getRegexMetchTopic().matcher(topic).find() && !isOneConsumerTopic(topic)){
      ClientSession clientSession = clientRegistrationRepository
          .findOne(producerExchange.getConnectionContext().getClientId());
      if (clientSession != null) {
        byte[] key = clientSession.getSessionKey();
        AES aes = new AES(key);
        ByteSequence payload = messageSend.getMessage().getContent();
        byte[] correctData = MQTTUtils.extractContent(payload.getData(), 0, payload.length);
        byte[] decryptedPayload = aes.decrypt(correctData);
        messageSend.getMessage().setContent(new ByteSequence(decryptedPayload));
      }
    }

    super.send(producerExchange, messageSend);
  }

  @Override
  public void preProcessDispatch(MessageDispatch messageDispatch) {

    String destinationName = messageDispatch.getDestination().getPhysicalName();
    logger.info("Pre Process Dispatch message topic " + destinationName);
    logger.trace("Message id " + messageDispatch.getMessage().getMessageId().getTextView());

    ConsumerId consumerId = messageDispatch.getConsumerId();
    Message message = messageDispatch.getMessage();
    try {
      if (messageDispatch.getDestination().isTopic() && internalTopic.contains(destinationName)) {
        ByteSequence content = updateMessageProducerInternal(message.getContent());
        message.setContent(content);
      } else if (messageDispatch.getDestination().isTopic() && !excludeTopic
          .contains(destinationName) && !MQTTUtils.getRegexMetchTopic().matcher(destinationName)
          .find()) {

        ByteSequence content = updateMessageToConsumer(consumerId,message.getContent());
        message.setContent(content);
      }

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return;
    }

    super.preProcessDispatch(messageDispatch);
  }

  @Override
  public Subscription addConsumer(ConnectionContext context, ConsumerInfo info) throws Exception {

    String connectionId = info.getConsumerId().getConnectionId();
    String clientId = getClientId(connectionId);

    String topic = info.getDestination().getPhysicalName();
    TopicSession topicSession = validateTopicToSubsribe(clientId,topic);

    if (isOneConsumerTopic(topic)) {
      String query = "org.apache.activemq:type=Broker,brokerName=" + getBrokerName() +
          ",destinationType=Topic,destinationName=" + topic + ",endpoint=Producer,*";
      ObjectName objectName = new ObjectName(query);
      Set<ObjectName> queryResult = this.getBrokerService().getManagementContext()
          .queryNames(objectName, null);
      if (!queryResult.isEmpty()) {
        logger.info("Try to add more consumer to topic " + topic + " Consumer refused");
        return null;
      }
    } else if (topicSession != null && StringUtils.isNotEmpty(topicSession.getId())) {
      ClientSession clientSession = clientRegistrationRepository.getOne(clientId);
      if (topicSession != null) {
        info.setDestination(new ActiveMQTopic(MQTTUtils.decodeTopicName(topicSession.getTopicName())));
        info.setSubscriptionName(clientId);
      }
    } else if (MQTTUtils.getRegexMetchTopic().matcher(topic).find()) {
      //do nothing
    } else {
      logger.info("Subscriber refused " + clientId);
      return null;
    }

    return super.addConsumer(context, info);
  }

  private boolean isOneConsumerTopic(String topic){
     return Stream
        .concat(excludeTopic.stream(), internalTopic.stream()).collect(Collectors.toList())
        .contains(topic);
  }

  private TopicSession validateTopicToSubsribe(String consumerId, String topicEncrypted) {
    try {
      byte[] key = getClientKey(consumerId);
      if (key != null) {
        AES aes = new AES(key);
        byte[] fromBase64 = CryptoUtils.decodeBase64(topicEncrypted.getBytes());
        String authToken = new String(aes.decrypt(fromBase64));
        if(MQTTUtils.validateToken(authToken,key,consumerId)) {
          String topic = MQTTUtils.getTopicFromAuthToken(authToken);
          if (StringUtils.isNotEmpty(topic)) {
             return topicSessionRepo.findOne(topic);
          }
        }
      }
    } catch (Exception e) {
      logger.error("Error during validateTopicToSubsribe "+e.getMessage(),e);
    }
    return null;
  }

  protected ByteSequence updateMessageToConsumer(ConsumerId consumerId,
      ByteSequence content) throws ActiveMQFilterException {
    byte[] cryptedContent = null;

    try {
      String consumerClientId = getClientId(consumerId.getConnectionId());
      byte[] consumerSessionKey = getClientKey(consumerClientId);

      logger.debug("consumerClientId = [" + consumerClientId + "]");
      byte [] data = MQTTUtils.extractContent(content.getData(),0,content.length);
      SymmetricAlgorithm symmetricConsumer = new AES(consumerSessionKey);
      cryptedContent = symmetricConsumer.encrypt(data);

    } catch (Exception e) {
      throw new ActiveMQFilterException(e.getMessage(), e);
    }

    return new ByteSequence(cryptedContent);
  }

  private String getClientId(String connectionID) throws Exception {
    Connection[] connections = getClients();
    for (Connection connection : connections) {
      TransportConnection transportConnection = (TransportConnection) connection;
      TransportConnectionState transportConnectionState = transportConnection
          .lookupConnectionState(new ConnectionId(connectionID));
      if (transportConnectionState != null) {
        String id = transportConnectionState.getContext().getConnectionId().getValue();
        if (id.equals(connectionID)) {
          return connection.getConnectionId();
        }
      }
    }

    return null;
  }

  private ByteSequence updateMessageProducerInternal(ByteSequence content) throws Exception {
    TopicSessionMessage topicSessionMessage = null;
    ByteSequence contentDecrypted =null;
    byte[] decryptedMesssage = null;

      byte [] correctData = MQTTUtils.extractContent(content.getData(),0,content.length);
      topicSessionMessage = JSONUtils.fromJson(new String(correctData));

      byte[] producerSessionKey = getClientKey(topicSessionMessage.getClientId());

      SymmetricAlgorithm symmetricProducer = new AES(producerSessionKey);
      decryptedMesssage = symmetricProducer.decrypt(topicSessionMessage.getPayload());

      //VALIDATE TOPIC for produce
      TopicSession payload = JSONUtils.fromJson(new String(decryptedMesssage));
      if(!payload.validateToken(producerSessionKey)) {
        throw new IllegalArgumentException("Invalid Auth1 Token");
      }
      contentDecrypted = new ByteSequence(JSONUtils.toJson(payload).getBytes());
      logger.debug("producerClientid = [" + topicSessionMessage.getClientId() + "]");

    return contentDecrypted ;
  }

  private byte[] getClientKey(String clientId) throws IllegalArgumentException {
    ClientSession clientSession = clientRegistrationRepository.findOne(clientId);
    if (clientSession != null) {
      return clientSession.getSessionKey();
    }

    return null;

  }
}