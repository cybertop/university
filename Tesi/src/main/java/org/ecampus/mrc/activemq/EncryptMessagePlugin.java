package org.ecampus.mrc.activemq;

import java.util.List;
import java.util.stream.Collectors;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.activemq.security.MessageAuthorizationPolicy;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.ecampus.mrc.repository.ClientRegistrationRepository;
import org.ecampus.mrc.repository.TopicSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class EncryptMessagePlugin implements BrokerPlugin {

  @Autowired
  private ClientRegistrationRepository clientRegistrationRepository;
  @Autowired
  private TopicSessionRepository topicSessionRepo;

  private List<String> excludeTopic;
  private List<String> internalTopic;

  private MessageAuthorizationPolicy authorizationPolicy;

  public Broker installPlugin(Broker broker) throws Exception {
    AMQBroker.init(broker);
    broker.getBrokerService().setMessageAuthorizationPolicy(authorizationPolicy);
    excludeTopic = decodeTopicName(excludeTopic);
    internalTopic = decodeTopicName(internalTopic);
    return new EncryptMessageFilter(broker,clientRegistrationRepository,topicSessionRepo, excludeTopic, internalTopic);
  }

  private List<String> decodeTopicName(List<String> internalTopic) {
    return internalTopic.stream().map(val -> MQTTUtils.decodeTopicName(val))
        .collect(Collectors.toList());
  }

  public List<String> getExcludeTopic() {
    return excludeTopic;
  }

  public void setExcludeTopic(List<String> excludeTopic) {
    this.excludeTopic = excludeTopic;
  }

  public List<String> getInternalTopic() {
    return internalTopic;
  }

  public void setInternalTopic(List<String> internalTopic) {
    this.internalTopic = internalTopic;
  }

  public MessageAuthorizationPolicy getAuthorizationPolicy() {
    return authorizationPolicy;
  }

  public void setAuthorizationPolicy(
      MessageAuthorizationPolicy authorizationPolicy) {
    this.authorizationPolicy = authorizationPolicy;
  }
}