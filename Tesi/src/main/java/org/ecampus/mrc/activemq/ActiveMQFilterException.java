package org.ecampus.mrc.activemq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActiveMQFilterException extends Exception {

  private final Logger LOG = LoggerFactory.getLogger(getClass());

  public ActiveMQFilterException() {
    super();
    LOG.error(this.getMessage(),this);
  }

  public ActiveMQFilterException(String message, Throwable cause) {
    super(message, cause);
    LOG.error(message,cause);
  }

  public ActiveMQFilterException(Throwable cause) {
    super(cause);
    LOG.error(cause.getMessage(),cause);
  }
}
