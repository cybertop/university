package org.ecampus.mrc.activemq;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.Message;
import org.apache.activemq.security.MessageAuthorizationPolicy;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EncryptAuthorizationPolicy implements MessageAuthorizationPolicy {

  private static final Log LOG = LogFactory.getLog(EncryptAuthorizationPolicy.class);

  private Map<String, String> policy;
  private final String EXTRACT_IP_REGEXP = "((\\d{1,3}.)){3}(\\d{1,3})";

  public EncryptAuthorizationPolicy(Map policy) {
    this.policy = policy;
  }

  @Override
  public boolean isAllowedToConsume(ConnectionContext context, Message message) {
    ActiveMQDestination destination = message.getDestination();
    if (destination.isTopic()) {
      String topic = destination.getPhysicalName();
      String ipValid = policy.get(topic);
      if (StringUtils.isNotEmpty(ipValid)) {
        String[] listIp = ipValid.split(";");
        String connection = context.getConnection().getRemoteAddress();
        if (Arrays.asList(listIp).contains(extractIp(connection))) {
          LOG.info("Permission to consume granted " + ipValid);
          return true;
        } else {
          LOG.info("Permission to consume denied " + connection);
          return false;
        }
      }
    }

    return true;
  }

  private String extractIp(String url) {
    Pattern pattern = Pattern.compile(EXTRACT_IP_REGEXP);
    Matcher matcher = pattern.matcher(url);
    if (matcher.find()) {
      return matcher.group(0);
    }

    return null;
  }
}
