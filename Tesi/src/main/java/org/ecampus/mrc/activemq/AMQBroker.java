package org.ecampus.mrc.activemq;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.activemq.command.ConsumerInfo;
import org.ecampus.mrc.mqtt.MQTTUtils;

public class AMQBroker implements BrokerConfiguration,DestinationFacade {


  private static AMQBroker amqBroker;
  private final BrokerService brokerService;
  private String brokerName;
  private String host;
  private int port;

  private Broker broker;

  private AMQBroker(Broker broker) {
    this.brokerName = broker.getBrokerName();
    URI uri = getConnectionParams(broker.getBrokerService().getTransportConnectors());
    this.host = uri.getHost();
    this.port = uri.getPort();
    this.brokerService = broker.getBrokerService();
    this.broker = broker;
  }

  private URI getConnectionParams(List<TransportConnector> connectorList) {
    final URI[] uri = new URI[1];
    connectorList.stream().filter(con -> con.getName().toUpperCase().contains("MQTT"))
        .forEach(con -> {
          try {
            uri[0] = con.getConnectUri();
          } catch (IOException e) {
            e.printStackTrace();
          } catch (URISyntaxException e) {
            e.printStackTrace();
          }
        });

    return uri[0];
  }

  protected static void init(Broker broker) {
    amqBroker = new AMQBroker(broker);
  }

  public static BrokerConfiguration getInstance() {
    return amqBroker;
  }

  public static DestinationFacade getDestinationFacade() {
    return amqBroker;
  }

  @Override
  public String getBrokerName() {
    return brokerName;
  }

  @Override
  public String getHost() {
    return host;
  }

  @Override
  public int getPort() {
    return port;
  }

  @Override
  public void removeTopic(String topic) throws Exception {
    broker.removeDestination(broker.getAdminConnectionContext(),new ActiveMQTopic( MQTTUtils.decodeTopicName(topic)),1000);
  }

  @Override
  public void createDestination(String topic) throws Exception {
    ActiveMQDestination destination = new ActiveMQTopic(topic);
    if(!existTopic(topic))
      broker.addDestination(broker.getAdminConnectionContext(),destination,true);
  }


  private boolean existTopic(String topic) throws Exception {

    Optional optional= Arrays.asList(broker.getDestinations()).stream()
        .filter(t -> t.isTopic()&& t.getPhysicalName().equals(topic)).findFirst();

    return optional.isPresent();
  }

  public void addSubscribe() throws Exception {
    broker.addConsumer(broker.getAdminConnectionContext(),new ConsumerInfo());
  }
}
