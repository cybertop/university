package org.ecampus.mrc.activemq;

public interface DestinationFacade {

  void removeTopic(String topic) throws Exception;
  void createDestination(String topic) throws Exception;
}
