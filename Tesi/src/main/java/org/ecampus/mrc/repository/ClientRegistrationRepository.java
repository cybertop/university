package org.ecampus.mrc.repository;

import org.ecampus.mrc.model.ClientSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRegistrationRepository extends JpaRepository<ClientSession,String> {

}
