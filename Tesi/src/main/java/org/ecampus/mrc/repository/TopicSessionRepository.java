package org.ecampus.mrc.repository;

import org.ecampus.mrc.model.TopicSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicSessionRepository extends JpaRepository<TopicSession,String> {

}
