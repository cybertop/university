package org.ecampus.mrc.model;

public interface GenericIdentifiable<T> {

  public T getId();
  public void setId(T id);
}
