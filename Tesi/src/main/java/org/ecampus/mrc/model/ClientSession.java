package org.ecampus.mrc.model;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ClientSession implements StringIdentifiable {


  @Id
  private String id;
  @Column
  protected byte[] sessionKey;
  @Column
  protected Date registrationDate;
  @Column
  protected boolean isValid;

  public ClientSession() {
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public byte[] getSessionKey() {
    return sessionKey;
  }

  public void setSessionKey(byte [] sessionKey) {
    this.sessionKey = sessionKey;
  }

  public Date getRegistrationDate() {
    return registrationDate;
  }

  public void setRegistrationDate(Date registrationDate) {
    this.registrationDate = registrationDate;
  }

  public boolean isValid() {
    return isValid;
  }

  public void setValid(boolean valid) {
    isValid = valid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ClientSession that = (ClientSession) o;

    if (isValid != that.isValid) {
      return false;
    }
    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (sessionKey != null ? !sessionKey.equals(that.sessionKey)
        : that.sessionKey != null) {
      return false;
    }
    return registrationDate != null ? registrationDate.equals(that.registrationDate)
        : that.registrationDate == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (sessionKey != null ? sessionKey.hashCode() : 0);
    result = 31 * result + (registrationDate != null ? registrationDate.hashCode() : 0);
    result = 31 * result + (isValid ? 1 : 0);
    return result;
  }
}
