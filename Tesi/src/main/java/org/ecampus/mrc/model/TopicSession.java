package org.ecampus.mrc.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.ecampus.mrc.mqtt.MQTTUtils;

@Entity
public class TopicSession implements StringIdentifiable {

  /**
   * Represent the secret code necessary to subscript to this topic
   */
  @Id
  private String id;
  @Column
  private String clientId;
  @Column
  private String authToken;
  @Column
  private String topicName;

  public TopicSession(){}

  public TopicSession(String topicName, byte[] encryptedMesssage){}

  public TopicSession(String topicCode, String clientId, String auth1) {
    this.id = topicCode;
    this.clientId = clientId;
    this.authToken = auth1;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String auth1) {
    this.authToken = auth1;
  }

  public String getTopicName() {
    return topicName;
  }

  public void setTopicName(String topicName) {
    this.topicName = topicName;
  }


  public boolean validateToken(byte[] key) throws Exception{
    boolean isValid = MQTTUtils.validateToken(authToken,key);
    if( isValid)
      topicName = MQTTUtils.getTopicFromAuthToken(authToken);

    return isValid;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TopicSession that = (TopicSession) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(clientId, that.clientId) &&
        Objects.equals(authToken, that.authToken) &&
        Objects.equals(topicName, that.topicName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, clientId, authToken, topicName);
  }
}
