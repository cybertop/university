package org.ecampus.mrc.model;

public class TopicSessionMessage {

  private String clientId;
  private byte[] payload;

  public TopicSessionMessage(){}

  public TopicSessionMessage(String clientId, byte[] payload) {
    this.clientId = clientId;
    this.payload = payload;
  }

  public String getClientId() {
    return clientId;
  }

  public byte[] getPayload() {
    return payload;
  }
}
