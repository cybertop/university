package org.ecampus.mrc.model;

public interface StringIdentifiable extends GenericIdentifiable<String> {

}
