package org.ecampus.mrc.crypto.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;


public class CyclicGroup {


  private BigInteger p, g,q;

  public void init(int size){
    SecureRandom random = new SecureRandom();
    BigInteger gen[] = generateSafePrimes(size,60,random);

    p = gen[0];
    q = gen [1];
    g = selectGenerator(p,q,random);

    System.out.println("------- "+this.getClass().getName()+" info -------");
    System.out.println("p mod q = " + p.mod(q).toString(16));
    System.out.println("g^{q} mod p = " + g.modPow(q,p).toString(16));
    System.out.println("p bit length = [" + p.bitLength() + "]");
    System.out.println("q bit length = [" + q.bitLength() + "]");
    System.out.println("g bit length = [" + g.bitLength() + "]");
    System.out.println();
  }

  public BigInteger getP() {
    return p;
  }

  public BigInteger getG() {
    return g;
  }

  public BigInteger getQ() {
    return q;
  }

  private  BigInteger[] generateSafePrimes(int size, int certainty, SecureRandom random) {
    BigInteger p, q;
    int qLength = size - 1;
    int minWeight = size >>> 2;

    for (;;)
    {
      q = new BigInteger(qLength, certainty, random);

      // p <- 2q + 1
      p = NumberUtils.TWO.multiply(q).add(NumberUtils.ONE);
      if (!p.isProbablePrime(certainty) && certainty > 2 && !q.isProbablePrime(certainty - 2))
      {
        continue;
      }

      if (getNafWeight(p) < minWeight)
      {
        continue;
      }

      break;
    }

    return new BigInteger[] { p, q };
  }

  private  int getNafWeight(BigInteger k) {
    if (k.signum() == 0)
    {
      return 0;
    }

    BigInteger _3k = k.shiftLeft(1).add(k);
    BigInteger diff = _3k.xor(k);

    return diff.bitCount();
  }

  private BigInteger selectGenerator(BigInteger p, BigInteger q, SecureRandom random)
  {
    BigInteger pMinusTwo = p.subtract(NumberUtils.TWO);
    BigInteger g,h;
    do
    {
      h = NumberUtils.generateRandomNumberInRange(NumberUtils.TWO, pMinusTwo, random);
      g = NumberUtils.generateGXModP(p,h,NumberUtils.TWO);
    }
    while (g.equals(NumberUtils.ONE));

    return g;
  }


  public ArrayList<BigInteger> getElements() {
    // This method is horribly ineffective for large groups and should not be used

    ArrayList<BigInteger> elements = new ArrayList<BigInteger>();

    BigInteger index = BigInteger.ONE;
    BigInteger element = BigInteger.ZERO;

    while (element.compareTo(BigInteger.ONE) != 0) {
      element = g.modPow(index, p);
      elements.add(element);

      index = index.add(BigInteger.ONE); // index++
    }

    return elements;
  }

  public static void main(String args [])
  {
    CyclicGroup d = new CyclicGroup();
    d.init(20);
    System.out.println("q = [" + d.getQ() + "]");
    System.out.println("p = [" + d.getP() + "]");
    System.out.println("g = [" + d.getG() + "]");
  }
}
