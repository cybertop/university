package org.ecampus.mrc.crypto.symmetric;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

public interface SymmetricAlgorithm {

  byte[] encrypt(byte[] obj) throws BadPaddingException, IllegalBlockSizeException;

  byte[] decrypt(byte[] obj) throws BadPaddingException, IllegalBlockSizeException;

  String getAlgorithm();
}
