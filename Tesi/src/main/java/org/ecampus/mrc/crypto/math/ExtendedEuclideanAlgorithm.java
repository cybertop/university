package org.ecampus.mrc.crypto.math;

import java.math.BigInteger;
import org.ecampus.mrc.crypto.utils.NumberUtils;

public class ExtendedEuclideanAlgorithm {


  public class BezoutCoefficients {

    private BigInteger x;
    private BigInteger y;
    private BigInteger gcd;

    public BezoutCoefficients(BigInteger x, BigInteger y, BigInteger gdc) {
      this.x = x;
      this.y = y;
      this.gcd = gdc;

    }

    public BigInteger getX() {
      return x;
    }

    public BigInteger getY() {
      return y;
    }

    public BigInteger getGcd() {
      return gcd;
    }

    @Override
    public String toString() {
      return "Bézout coefficients " + x + " /\\ " + y + " \n greatest common divisor:" + gcd;
    }
  }


  public BezoutCoefficients calculate(BigInteger a, BigInteger q) {
    BigInteger s = NumberUtils.ZERO;
    BigInteger old_s = NumberUtils.ONE;
    BigInteger t = NumberUtils.ONE;
    BigInteger old_t = NumberUtils.ZERO;

    BigInteger r = q;
    BigInteger old_r = a;
    BigInteger quotient;

    while (!r.equals(NumberUtils.ZERO)) {
      quotient = old_r.divide(r);
      BigInteger tmp = r;
      r = old_r.subtract(quotient.multiply(tmp));
      old_r = tmp;

      tmp = s;
      s = old_s.subtract(quotient.multiply(tmp));
      old_s = tmp;

      tmp = t;
      t = old_t.subtract(quotient.multiply(tmp));
      old_t = tmp;


    }
    return new BezoutCoefficients(old_s, old_t, old_r);
  }

}
