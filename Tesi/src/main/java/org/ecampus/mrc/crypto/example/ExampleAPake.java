package org.ecampus.mrc.crypto.example;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.ecampus.mrc.Utils;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.GroupGenerator;
import org.ecampus.mrc.crypto.apake.APakePayload;
import org.ecampus.mrc.crypto.apake.PakeClient;
import org.ecampus.mrc.crypto.apake.PakeServer;
import org.ecampus.mrc.crypto.symmetric.AES;
import org.ecampus.mrc.json.JSONUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExampleAPake {

  private static final String clientID = "client";
  private static final String serverID = "server";
  private static final String password = "passwoed1";

  private static final Logger LOG = LoggerFactory.getLogger(ExampleAPake.class);

  public static void main (String args[]){
    GroupGenerator generator = new GroupGenerator(1024,200);
    try {
      DSAGroupParams group = generator.generateParameters();
      PakeClient client = new PakeClient(group,password,clientID,serverID);
      PakeServer server = new PakeServer(group,serverID);

      //Registration
      Date a = new Date();
      APakePayload registrationPayload = client.generateRegistrationPaylod();
      System.out.println("MSG Regs: "+ Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS ) + " MILLISECONDS");
      String json = JSONUtils.toJson(registrationPayload);
    //  APakePayload a = JSONUtils.fromJson(json);
      server.registration(registrationPayload);

      //Step 1
      System.out.println("Fase 1");
      a = new Date();
      APakePayload X = client.generateX();
      System.out.println("MSG X: "+Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS )+ " MILLISECONDS");

      a = new Date();
      APakePayload Y = server.generateY(X);
      System.out.println("MSG Y: "+Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS )+ " MILLISECONDS" );


      //Step 2
      System.out.println("Fase 2");
      a = new Date();
      APakePayload V_U = client.generateVU(Y);
      System.out.println("MSG V_U: "+Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS )+ " MILLISECONDS" );

      a = new Date();
      APakePayload v_S = server.generateVSandSK(V_U);
      System.out.println("MSG v_S: "+Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS )+ " MILLISECONDS" );

      //Step 3
      System.out.println("Fase 3");
      a = new Date();
      client.computeSessionKey(v_S);
      System.out.println("MSG Computa v_S: "+Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS ) + " MILLISECONDS" );

      a = new Date();
      byte[] sk_server = server.getSessionKey();
      System.out.println("Generate server key: "+Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS ) + " MILLISECONDS" );

      a = new Date();
      byte[]  sk_client = client.getSessionKey();
      System.out.println("Generate client key: "+Utils.getDateDiff(a,new Date(), TimeUnit.MILLISECONDS ) + " MILLISECONDS" );

      System.out.println("Server key -> "+sk_server);
      System.out.println("Client key -> "+sk_client);

      AES aes = new AES(sk_client);
      byte[] encrypted = aes.encrypt("Ciao MArco".getBytes());
      System.out.println(new String(aes.decrypt(encrypted)));

    } catch (Exception e) {
      LOG.error("Computation error",e);
    }

  }

}
