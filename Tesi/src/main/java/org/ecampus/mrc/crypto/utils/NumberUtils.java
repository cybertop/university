package org.ecampus.mrc.crypto.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public class NumberUtils {


  public static final BigInteger ZERO = BigInteger.valueOf(0);
  public static final BigInteger ONE  = BigInteger.valueOf(1);
  public static final BigInteger TWO  = BigInteger.valueOf(2);
  public static final BigInteger MINUS_ONE = BigInteger.valueOf(-1);

  public static boolean isProbablePrime(BigInteger x,int certainty)
  {
    return x.isProbablePrime(certainty);
  }


  public static BigInteger generateRandomNumberModuleQ(BigInteger q, SecureRandom random){

    BigInteger n;
    do {
      n = new BigInteger(q.bitLength(),random);
      n = n.mod(q);

    }while(n.compareTo(ZERO) == -1 || !n.isProbablePrime(80));

    return n;
  }

  public static BigInteger generateRandomNumberInRange(BigInteger min,BigInteger max,SecureRandom random){
    if(min.compareTo(max) >= 0 ) throw new IllegalArgumentException( "Values not valid. Min must be less he Max");

    return new BigInteger(max.subtract(min).bitLength() - 1, random).add(min);

  }

  /**
   *  if n < 0  then return exp_by_squaring(1 / x, -n);
      else if n = 0  then return  1;
      else if n = 1  then return  x ;
      else if n is even  then return exp_by_squaring(x * x,  n / 2);
      else if n is odd  then return x * exp_by_squaring(x * x, (n - 1) / 2);
    * @param x
   * @param n
   * @return
   */
  public static BigInteger expBySquaring(BigInteger x, BigInteger n){
    System.out.println("x = [" + x + "], n = [" + n + "]");
    if(n.compareTo(ZERO) == -1 ) return ONE;
    else if(n.equals(ONE)) return x;
    else if(!isOdd(n)) return expBySquaring(x.multiply(x),n.divide(TWO));
    else if (isOdd(n))return x.multiply(expBySquaring(x.multiply(x),n.subtract(ONE).divide(TWO)));

    return null;
  }

  public static boolean isOdd(BigInteger val) {
    return !val.mod(new BigInteger("2")).equals(BigInteger.ZERO) ;
  }
  /**
   * Calculate g^x mod p.
   */
  public static BigInteger generateGXModP (BigInteger p,
      BigInteger g,
      BigInteger x){

    return g.modPow(x,p);

  }

  public static void main (String args []){

    System.out.println("args = [" + expBySquaring(new BigInteger(String.valueOf("349649874984449749")),new BigInteger(String.valueOf("349649874984449749")) ) + "]");
  }
}
