package org.ecampus.mrc.crypto;

import java.math.BigInteger;
import java.security.interfaces.DSAParams;

import java.security.spec.AlgorithmParameterSpec;
import java.util.Properties;
import org.ecampus.mrc.crypto.utils.NumberUtils;

public class DSAGroupParams implements DSAParams, AlgorithmParameterSpec {


  private final BigInteger p;
  private final BigInteger q;
  private final BigInteger g;

  public DSAGroupParams( BigInteger p, BigInteger q,  BigInteger g) {
    this.p = p;
    this.g = g;
    this.q = q;
  }

  public DSAGroupParams(Properties properties){
    this.p = new BigInteger(properties.getProperty("p"));
    this.q = new BigInteger(properties.getProperty("q"));
    this.g = new BigInteger(properties.getProperty("g"));
  }

  public boolean isValid(){

    if(!getP().subtract(NumberUtils.ONE).mod(getQ()).equals(NumberUtils.ZERO))return false;
    if(getG().compareTo(NumberUtils.TWO) == -1 ) return false;
    if(getG().compareTo(getP().subtract(NumberUtils.ONE)) == 1) return false;
    if(!getG().modPow(getQ(), getP()).equals(NumberUtils.ONE))return false;


    return true;

  }

  @Override
  public String toString() {
    return new StringBuffer()
              .append("G: ")
              .append(getG())
              .append("\n P: ")
              .append(getP())
              .append("\n Q: ")
              .append(getQ()).toString();
  }

  @Override
  public BigInteger getP() {
    return p;
  }

  @Override
  public BigInteger getQ() {
    return q;
  }

  @Override
  public BigInteger getG() {
    return g;
  }
}
