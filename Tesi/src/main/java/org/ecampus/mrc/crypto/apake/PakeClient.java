package org.ecampus.mrc.crypto.apake;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.math.ExtendedEuclideanAlgorithm;
import org.ecampus.mrc.crypto.math.ExtendedEuclideanAlgorithm.BezoutCoefficients;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.ecampus.mrc.crypto.utils.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PakeClient {

  private final BigInteger password;
  private final String clientID;
  private final String serverID;
  private final DSAGroupParams groupParams;
  private MessageDigest md;
  private BigInteger x;
  private BigInteger X;
  private byte[] SK;
  private BigInteger Y;
  private BigInteger K;
  private BigInteger w1;

  private static final Logger LOG = LoggerFactory.getLogger(PakeClient.class);

  public PakeClient(DSAGroupParams groupParams, String password, String clientID, String serverID)
      throws NoSuchAlgorithmException {
    this(groupParams, password, clientID, serverID, MessageDigest.getInstance("SHA-256"));

  }

  public PakeClient(DSAGroupParams groupParams, String password, String clientID, String serverID,
      MessageDigest md) {
    LOG.info("INIT");
    this.password = new BigInteger(password.getBytes());
    this.clientID = clientID;
    this.serverID = serverID;

    this.groupParams = groupParams;
    this.md = md;
  }

  public APakePayload generateRegistrationPaylod() {
    LOG.info("Start generate Registration Payload");
    List<BigInteger> val = Arrays.asList(new BigInteger(clientID.getBytes()),
        new BigInteger(serverID.getBytes()),
        new BigInteger(String.valueOf(password)));
    this.w1 = new BigInteger(CryptoUtils.getHashBigInteger(md, val));
    BigInteger W = groupParams.getG().modPow(w1, groupParams.getP());

    return new APakePayload(clientID, W, new Date(), PakeStep.REGISTER);
  }

  /**
   * Fase 1 U-->S: (U,X)
   *
   * x = random from Z_q^* X = g^x mod p
   */
  public APakePayload generateX() {
    LOG.info("Generate X");
    this.x = NumberUtils.generateRandomNumberModuleQ(groupParams.getQ(), new SecureRandom());
    X = groupParams.getG().modPow(x, groupParams.getP());

    return new APakePayload(clientID, X, new Date(), PakeStep.CLIENT_X);
  }


  /**
   * FASE 3 U-->S: V_U
   *
   * r = H(0x01!|U|S|X) z = 1 / (x + (w * r)) mod p K = Y^z mod p
   *
   * V_U = H(0x02|U|S|X|Y|K)
   */
  public APakePayload generateVU(APakePayload in) throws IllegalArgumentException {

    LOG.info("Received Y");

    if (!in.getId().equals(serverID)) {
      new IllegalArgumentException("Invalid server ID");
    }

    this.Y = in.getValue();

    if (Y.mod(groupParams.getP()).equals(NumberUtils.ZERO) ||
        Y.mod(groupParams.getP()).equals(NumberUtils.ONE) ||
        Y.mod(groupParams.getP()).equals(NumberUtils.MINUS_ONE)) {
      throw new IllegalArgumentException("Invalid Y");
    }

    List<BigInteger> val = Arrays
        .asList(new BigInteger(CryptoUtils.char1), new BigInteger(clientID.getBytes()),
            new BigInteger(serverID.getBytes()), X);

    K = Y.modPow(generateZ(val), groupParams.getP());

    val = Arrays.asList(new BigInteger((CryptoUtils.char2)), new BigInteger(clientID.getBytes()),new BigInteger(serverID.getBytes()), X, Y, K);

    BigInteger V_U = new BigInteger(CryptoUtils.getHashBigInteger(md, val));
    /*
    System.out.println("################ CLLIENT ################\n"
        + "1 - "+ new BigInteger((CryptoUtils.char2)) +"\n"
        + "2 CLIENT ID - "+ new BigInteger(clientID.getBytes()) +"\n"
        + "3 - SERVER ID - "+ new BigInteger(serverID.getBytes()) +"\n"
        + "4 X - "+ X +"\n"
        + "5 Y - "+ Y +"\n"
        + "5 K - "+ K +"\n"
    );
  */

    LOG.info("Generate V_U");

    return new APakePayload(clientID, V_U, new Date(), PakeStep.CLIENT_VU);
  }

  /**
   * Fase 5
   *
   * SK = = H(0x04|U|S|X|Y|K)
   */
  public void computeSessionKey(APakePayload in) throws IllegalArgumentException {
    LOG.info("Received V_S");
    if (!in.getId().equals(serverID)) {
      new IllegalArgumentException("Invalid server ID");
    }

    List<BigInteger> val = Arrays
        .asList(new BigInteger((CryptoUtils.char3)), new BigInteger(clientID.getBytes()),
            new BigInteger(serverID.getBytes()), X, Y, K);
    BigInteger V_S_Client = new BigInteger(CryptoUtils.getHashBigInteger(md, val));
    //check V_S
    if (!in.getValue().equals(V_S_Client)) {
      throw new IllegalArgumentException("Invalid V_S");
    }

    // Session Key for client
    val = Arrays.asList(new BigInteger((CryptoUtils.char4)), new BigInteger(clientID.getBytes()),
        new BigInteger(serverID.getBytes()), X, Y, K);
    SK = CryptoUtils.getHashBigInteger(md,val);
    //System.out.println("SIZE "+v.length);
    //SK = new String (Hex.encodeHex(v));
    LOG.info("Session Key generated");

    resetParams();

  }

  public byte [] getSessionKey() {
    return SK;
  }

  private void resetParams() {
    LOG.info("Reset private parameters");
    this.Y = null;
    K = null;
    x = null;
    X = null;
  }

  private BigInteger generateZ(List<BigInteger> val) {
    BigInteger r = new BigInteger(CryptoUtils.getHashBigInteger(md, val));
    BigInteger a = x.add(w1.multiply(r));
    ExtendedEuclideanAlgorithm eea = new ExtendedEuclideanAlgorithm();
    BezoutCoefficients calculate = eea.calculate(a, groupParams.getQ());
    return calculate.getX();
  }
}
