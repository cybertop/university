package org.ecampus.mrc.crypto.pake;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.DSAParams;
import org.ecampus.mrc.crypto.DSAGroupParams;

public class UserInfo {

  private String id;
  private String password;
  private DSAGroupParams groupParams;
  private SecureRandom random;
  private MessageDigest digest;

  public UserInfo(
      String id,
      String password,
      DSAParams group) throws NoSuchAlgorithmException {
    this(id,password,group,new SecureRandom(),MessageDigest.getInstance("SHA-256"));

  }
  public UserInfo(
      String id,
      String password,
      DSAParams group,
      SecureRandom random,
      MessageDigest digest){

      if(id == null || password == null || password.isEmpty()  || group == null || random == null)
        throw new NullPointerException("Input must not be null");

      this.id = id;
      this.password = password;
      this.groupParams = (DSAGroupParams) group;
      this.random = random;
      this.digest = digest;

  }

  public String getPassword() {
    return password;
  }

  public DSAGroupParams getGroupParams() {
    return groupParams;
  }

  public SecureRandom getRandom() {
    return random;
  }

  public MessageDigest getMessageDigest() {
    return digest;
  }

  public String getId() {
    return id;
  }
}
