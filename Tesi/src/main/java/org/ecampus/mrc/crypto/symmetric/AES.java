package org.ecampus.mrc.crypto.symmetric;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class AES extends BaseSymmetricAlgorithm {

  public AES(byte[] key)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
    super("AES");
    Key keyy = new SecretKeySpec(key, "AES");
    cipherEncrypt.init(Cipher.ENCRYPT_MODE, keyy);
    cipherDecrypt.init(Cipher.DECRYPT_MODE, keyy);
  }

}
