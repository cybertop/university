package org.ecampus.mrc.crypto.symmetric;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Arrays;
import java.util.Set;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class TripleDES extends BaseSymmetricAlgorithm {

  private final int KEY_SIZE = 24;

  /**
   * Use DESede/ECB/PKCS5Padding Algorithm.
   * Requires key with a maximum size of 24 bits, if it is longer,
   * cut the ky to the 24th bit.
   *
   * @param key
   * @throws NoSuchAlgorithmException
   * @throws NoSuchPaddingException
   * @throws InvalidKeyException
   */
  public TripleDES(byte[] key)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
    super("DESede/ECB/PKCS5Padding");

    key = validateKey(key);
    Key keyy = new SecretKeySpec(key, "DESede");
    cipherEncrypt.init(Cipher.ENCRYPT_MODE, keyy);
    cipherDecrypt.init(Cipher.DECRYPT_MODE, keyy);
  }

  private byte[] validateKey(byte [] key) throws InvalidKeyException {
    byte [] newKey = null;
    if (key.length  < KEY_SIZE) {
      throw new InvalidKeyException("Wrong key size");
    } else {
      newKey = new byte[24];
      System.arraycopy(key, 0, newKey, 0, KEY_SIZE);
    }

    return newKey;
  }
}
