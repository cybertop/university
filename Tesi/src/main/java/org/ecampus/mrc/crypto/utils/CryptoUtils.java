package org.ecampus.mrc.crypto.utils;

import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang3.ArrayUtils;

public class CryptoUtils {

  public static final byte[] char1 = {0x01};
  public static final byte[] char2 = {0x02};
  public static final byte[] char3 = {0x03};
  public static final byte[] char4 = {0x04};

  private static final String MAC_ALGORITHM = "HmacMD5";

  public static byte[] getHashBigInteger(MessageDigest md, List<BigInteger> val) {
    md.reset();
    val.forEach(item -> md.update(item.toByteArray()));
    return md.digest();
  }

  public static byte[] getHash(MessageDigest md, byte[] val) {
    md.reset();
    return md.digest(val);
  }

  public static byte[] getHash(MessageDigest md, List<byte[]> val) {
    md.reset();
    val.forEach(item -> md.update(item));
    return md.digest();
  }

  public static boolean verifyMAC( byte[] key,List<String> values,byte[] macSource)
      throws InvalidKeyException, NoSuchAlgorithmException {

    if (ArrayUtils.isEmpty(key) || values ==null || values.isEmpty()) {
      throw new IllegalArgumentException("All inputs must be no null or empty");
    }
     byte digset [] = getMAC(key,values);

    return Arrays.equals(macSource, digset);
  }

  public static byte[] getMAC(byte[] key,List<String> values)
      throws NoSuchAlgorithmException, InvalidKeyException {
    Mac mac = Mac.getInstance(MAC_ALGORITHM);
    SecretKey keySpec = new SecretKeySpec(key, MAC_ALGORITHM);
    mac.init(keySpec);
    values.forEach(item -> mac.update(item.getBytes()));
    return mac.doFinal();
  }

  public static boolean verify(MessageDigest md, byte[] hash, String values[]) {
    if (md == null || ArrayUtils.isEmpty(hash) || ArrayUtils.isEmpty(values)) {
      throw new IllegalArgumentException("All inputs must be no null or empty");
    }

    md.reset();
    Arrays.stream(values).forEach(i -> md.update(i.getBytes()));
    byte tmp[] = md.digest();

    return Arrays.equals(hash, tmp);
  }

  public static double getVersion () {
    double version = Double.parseDouble(System.getProperty("java.specification.version"));
    return version;
  }

  public static byte[] decodeBase64(byte[] in){
    //Use URL Decode to avoid '/' it's especial char for the topic name
    return Base64.getUrlDecoder().decode(in);

  }

  public static byte[] encodeBase64(byte[] in){
    return  Base64.getUrlEncoder().encode(in);
  }

}
