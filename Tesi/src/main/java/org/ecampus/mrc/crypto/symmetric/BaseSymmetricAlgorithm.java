package org.ecampus.mrc.crypto.symmetric;

import java.lang.reflect.Field;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.ecampus.mrc.crypto.utils.CryptoUtils;

public abstract class BaseSymmetricAlgorithm implements SymmetricAlgorithm {
  protected Cipher cipherEncrypt = null;
  protected Cipher cipherDecrypt = null;

  private final String algorithm;

  protected BaseSymmetricAlgorithm(String algorithm)
      throws NoSuchPaddingException, NoSuchAlgorithmException,InvalidParameterException {
    this.algorithm = algorithm;
    disableJceLimit();
    cipherEncrypt = Cipher.getInstance(algorithm);
    cipherDecrypt = Cipher.getInstance(algorithm);
  }

  @Override
  public byte[] encrypt (byte [] obj) throws BadPaddingException, IllegalBlockSizeException {
    return cipherEncrypt.doFinal(obj);
  }

  @Override
  public byte[] decrypt (byte [] obj) throws BadPaddingException, IllegalBlockSizeException {
    return cipherDecrypt.doFinal(obj);
  }

  @Override
  public String getAlgorithm() {return algorithm;}


  private void disableJceLimit() throws InvalidParameterException {

    double version = CryptoUtils.getVersion();
    if (version <= 1.8) {
      try {
        Field field = Class.forName("javax.crypto.JceSecurity").
            getDeclaredField("isRestricted");
        field.setAccessible(true);
        field.set(null, java.lang.Boolean.FALSE);
      } catch (Exception e) {
        throw new InvalidParameterException("Error JCE Security Restriction");
      }
    }
    if(version >= 10)
      Security.setProperty("crypto.policy", "unlimited");

  }
}
