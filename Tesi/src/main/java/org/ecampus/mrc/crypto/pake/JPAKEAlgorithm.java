package org.ecampus.mrc.crypto.pake;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.interfaces.DSAParams;
import org.ecampus.mrc.crypto.utils.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JPAKEAlgorithm {

  private static final Logger LOG = LoggerFactory.getLogger(JPAKEAlgorithm.class);

  private UserInfo userInfo;
  private BigInteger x1;
  private BigInteger x2;
  private PayloadPhase1 phase1Send;

  public JPAKEAlgorithm(UserInfo userInfo) {
    this.userInfo = userInfo;
  }

  /**
   * Step 1: Alice sends g^{x1}, g^{x2}, and Bob sends g^{x3}, g^{x4}
   */
  public PayloadPhase1 createPayloadPhase1() {
    LOG.info("Step 1 create Payload");
    DSAParams parmas = userInfo.getGroupParams();
    x1 = generateX1(parmas.getQ(), userInfo.getRandom());
    x2 = generateX2(parmas.getQ(), userInfo.getRandom());

    LOG.debug("x1: "+x1);
    LOG.debug("x2: "+x2);

    BigInteger gx1 = NumberUtils.generateGXModP(parmas.getP(), parmas.getG(), x1);
    BigInteger gx2 = NumberUtils.generateGXModP(parmas.getP(), parmas.getG(), x2);

    BigInteger[] sigX1 = generateZKP(parmas.getP(), parmas.getQ(), parmas.getG(), gx1, x1,userInfo);
    BigInteger[] sigX2 = generateZKP(parmas.getP(), parmas.getQ(), parmas.getG(), gx2, x2,userInfo);

    this.phase1Send = new PayloadPhase1(userInfo.getId(), gx1, gx2, sigX1, sigX2);

    return phase1Send;
  }

  /**
   * PayloadPhase1 received from Bob
   */
  public PayloadPhase2 createPayloadPhase2(PayloadPhase1 phase1Received) throws IllegalArgumentException{
    LOG.info("Step 2");
    LOG.info("Received Payload Phase 1");
    LOG.info("Generate Payload Phase 2");

    DSAParams parmas = userInfo.getGroupParams();

    if (validatePayloadPhase1(phase1Received)) {

      BigInteger gA = phase1Send.getGx1().multiply(phase1Received.getGx1())
          .multiply(phase1Received.getGx2()).mod(parmas.getP());
      BigInteger s = new BigInteger(userInfo.getPassword().getBytes());
      BigInteger x2s = x2.multiply(s).mod(parmas.getQ());
      BigInteger A = gA.modPow(x2s, parmas.getP());

      BigInteger[] sigX2s = generateZKP(parmas.getP(), parmas.getQ(), gA, A, x2s, userInfo);

      return new PayloadPhase2(userInfo.getId(), A, sigX2s);
    } else {
      throw new IllegalArgumentException("Invalid received payload.");
    }
  }

  public BigInteger generateKeyMaterial(PayloadPhase1 phase1Received,
      PayloadPhase2 phase2Received)throws IllegalArgumentException {

    LOG.info("Step 3 ");
    LOG.info("Generate Key Material");
    DSAParams parmas = userInfo.getGroupParams();
    if (validatePayloadPhase2(phase1Send, phase1Received, phase2Received)) {
      BigInteger s = new BigInteger(userInfo.getPassword().getBytes());
      BigInteger key = phase1Received.getGx2()
          .modPow(x2.multiply(s).negate().mod(parmas.getQ()), parmas.getP())
          .multiply(phase2Received.getA()).modPow(x2, parmas.getP());
      userInfo.getMessageDigest().reset();
      userInfo.getMessageDigest().update(key.toByteArray());
      x1 = null;
      x2 = null;
      return new BigInteger(1, userInfo.getMessageDigest().digest());
    } else {
      throw new IllegalArgumentException("Invalid received payload.");
    }

  }


  /******Private Method *******/

  private boolean validatePayloadPhase1(PayloadPhase1 phase1) {
    if (phase1.getGx2().equals(NumberUtils.ONE) ||
        !isValidZKP(userInfo.getGroupParams().getP(), userInfo.getGroupParams().getQ(),
            userInfo.getGroupParams().getG(), phase1.gx1, phase1.getSigX1(), phase1.getId()) ||
        !isValidZKP(userInfo.getGroupParams().getP(), userInfo.getGroupParams().getQ(),
            userInfo.getGroupParams().getG(), phase1.gx2, phase1.getSigX2(), phase1.getId())) {
      return false;
    }

    return true;
  }

  private boolean validatePayloadPhase2(PayloadPhase1 phase1Send, PayloadPhase1 phase1Received,
      PayloadPhase2 phase2Received) {
    DSAParams parmas = userInfo.getGroupParams();
    // ga = g^(x1+x3+x4) = g^x1 * g^x3 * g^x4
    BigInteger gB = phase1Received.getGx1().multiply(phase1Send.getGx1()).multiply(phase1Send.getGx2()).mod(parmas.getP());

    if (gB.equals(NumberUtils.ONE) ||
        !isValidZKP(userInfo.getGroupParams().getP(),
            userInfo.getGroupParams().getQ(),
            gB,
            phase2Received.getA(),
            phase2Received.getSigX2s(), phase1Received.getId())) {
      return false;
    }

    return true;
  }

  /**
   * Calculate random number from [0,q-1]
   */
  private static BigInteger generateX1(BigInteger q, SecureRandom random) {
    BigInteger min = NumberUtils.ZERO;
    BigInteger max = q.subtract(NumberUtils.ONE);

    return NumberUtils.generateRandomNumberInRange(min, max, random);
  }

  /**
   * Calculate random number from [1,q-1]
   */
  private static BigInteger generateX2(BigInteger q, SecureRandom random) {
    BigInteger min = NumberUtils.ONE;
    BigInteger max = q.subtract(NumberUtils.ONE);

    return NumberUtils.generateRandomNumberInRange(min, max, random);
  }

  private static BigInteger[] generateZKP(BigInteger p, BigInteger q, BigInteger g, BigInteger gx,
      BigInteger x, UserInfo userInfo) {

    BigInteger[] zkp = new BigInteger[2];

    /* Generate a random v, and compute g^v */
    BigInteger v = NumberUtils
        .generateRandomNumberInRange(NumberUtils.ZERO, p.subtract(NumberUtils.ONE),
            userInfo.getRandom());
    BigInteger gv = g.modPow(v, p);

    userInfo.getMessageDigest().reset();
    userInfo.getMessageDigest().update(g.toByteArray());
    userInfo.getMessageDigest().update(gv.toByteArray());
    userInfo.getMessageDigest().update(gx.toByteArray());
    userInfo.getMessageDigest().update(userInfo.getId().getBytes());

    BigInteger h = new BigInteger(userInfo.getMessageDigest().digest());
    zkp[0] = gv;
    // r = v-x*h
    zkp[1] = v.subtract(x.multiply(h)).mod(q);

    return zkp;
  }

  private boolean isValidZKP(BigInteger p, BigInteger q, BigInteger g, BigInteger gx,
      BigInteger[] sig, String id) {

    userInfo.getMessageDigest().reset();
    userInfo.getMessageDigest().update(g.toByteArray());
    userInfo.getMessageDigest().update(sig[0].toByteArray());
    userInfo.getMessageDigest().update(gx.toByteArray());
    userInfo.getMessageDigest().update(id.getBytes());

    BigInteger h = new BigInteger(userInfo.getMessageDigest().digest());

    if (gx.compareTo(BigInteger.ZERO) == 1 && // g^x > 0
        gx.compareTo(p.subtract(BigInteger.ONE)) == -1 && // g^x < p-1
        gx.modPow(q, p).compareTo(BigInteger.ONE) == 0 && // g^x^q = 1
        g.modPow(sig[1], p).multiply(gx.modPow(h, p)).mod(p).compareTo(sig[0])== 0) // g^v=g^r * g^x^h
    {
      return true;
    }

    return false;
  }


  /************* Private Class ********/

  public class PayloadPhase1 {

    private String id;
    private BigInteger gx1;
    private BigInteger gx2;
    private BigInteger[] sigX1;
    private BigInteger[] sigX2;

    public PayloadPhase1(String id, BigInteger gx1, BigInteger gx2, BigInteger[] sigX1,
        BigInteger[] sigX2) {
      this.id = id;
      this.gx1 = gx1;
      this.gx2 = gx2;
      this.sigX1 = sigX1;
      this.sigX2 = sigX2;
    }

    public String getId() {
      return id;
    }

    public BigInteger getGx1() {
      return gx1;
    }

    public BigInteger getGx2() {
      return gx2;
    }

    public BigInteger[] getSigX1() {
      return sigX1;
    }

    public BigInteger[] getSigX2() {
      return sigX2;
    }
  }

  public class PayloadPhase2 {

    private String id;
    private BigInteger a;
    private BigInteger[] sigX2s;

    public PayloadPhase2(String id, BigInteger a, BigInteger[] sigX2s) {
      this.id = id;
      this.a = a;
      this.sigX2s = sigX2s;
    }

    public String getId() {
      return id;
    }

    public BigInteger getA() {
      return a;
    }

    public BigInteger[] getSigX2s() {
      return sigX2s;
    }
  }

}



