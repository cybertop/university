package org.ecampus.mrc.crypto;

import java.security.AlgorithmParameterGenerator;
import java.security.AlgorithmParameters;
import java.security.DigestException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.spec.DSAGenParameterSpec;
import java.security.spec.DSAParameterSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *  group G with generator g of prime order  q in which the discrete log problem is hard.
 */
public class GroupGenerator {


  private static final Logger LOG = LoggerFactory.getLogger(GroupGenerator.class);

  /**
   * certainty a measure of the uncertainty that the caller is willing to tolerate: if the call
   * returns {@code true} the probability that this BigInteger is prime exceeds (1 - 1/2<sup>{@code
   * certainty}</sup>).
   */

  private int sizeP, sizeQ;
  private SecureRandom random;
  private MessageDigest messageDigest;
  private  int seed;

  public GroupGenerator(int sizeP,int seed)
  {
    this(sizeP,seed,new SecureRandom(),null);
  }

  public GroupGenerator(int sizeP,int seed, SecureRandom random, MessageDigest messageDigest) {

    LOG.info("INIT");
    this.sizeP = sizeP;
    this.sizeQ = getN(this.sizeP);
    this.random = random;
    this.seed = seed;
    this.messageDigest = messageDigest;

    checkStrength(this.sizeP, sizeQ,this.seed);
  }

  public DSAGroupParams generateParameters() throws Exception {

      LOG.info("Algorithm Parameter Generator DSA");
      AlgorithmParameterGenerator algorithmParameterGenerator = AlgorithmParameterGenerator
          .getInstance("DSA");

      LOG.info("Size p: "+sizeP+" Size q: "+sizeQ+" seed: "+seed);
      DSAGenParameterSpec genParameterSpec = new DSAGenParameterSpec(sizeP, sizeQ,seed);
      algorithmParameterGenerator.init(genParameterSpec, random);

      AlgorithmParameters algorithmParameters = algorithmParameterGenerator.generateParameters();
      DSAParameterSpec dsaGroupParams = algorithmParameters
          .getParameterSpec(DSAParameterSpec.class);

      return new DSAGroupParams(dsaGroupParams.getP(),dsaGroupParams.getQ(),dsaGroupParams.getG());
  }

  private static int getN(int modlen)
  {
    return modlen > 1024 ? 256 : 160;
  }

  private static void checkStrength(int sizeP, int sizeQ,int seed) throws IllegalArgumentException {
    if ((sizeP < 512 || sizeP > 1024 || sizeP % 64 != 0 || sizeQ != 160) && (sizeP != 2048 || sizeQ != 224 && sizeQ != 256)) {
      throw new InvalidParameterException("Prime length not supported: " + sizeP + ", " + sizeQ);
    }
    if(seed < sizeQ )
      throw new InvalidParameterException("seed must be equal to or greater then Q : " + seed + ", " + sizeQ);
  }

  public static void main(String args[]) {


    try {

      GroupGenerator g = new GroupGenerator(1024,200);
      DSAGroupParams p = g.generateParameters();
      System.out.println("Parms = [" + p.toString() + "]");
      System.out.println("Is Valid = [" + p.isValid() + "]");


    } catch (DigestException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }


  }

}

