package org.ecampus.mrc.crypto.example;

import java.math.BigInteger;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.GroupGenerator;
import org.ecampus.mrc.crypto.pake.JPAKEAlgorithm;
import org.ecampus.mrc.crypto.pake.JPAKEAlgorithm.PayloadPhase1;
import org.ecampus.mrc.crypto.pake.JPAKEAlgorithm.PayloadPhase2;
import org.ecampus.mrc.crypto.pake.UserInfo;

public class ExampleTestPake {


  public static void main(String arg[]){
    /***************** Test genera chiavi **********************/
    DSAGroupParams params = null;
    try {
      GroupGenerator generator = new GroupGenerator(1024,200);
       params = generator.generateParameters();

      System.out.println(params.toString());
      System.out.println("Is Valid:" +params.isValid());

    } catch (Exception e) {
      System.out.print( e.getMessage());
    }

    /***************** Test PAKE Algorithm **********************/
    try{
      UserInfo alice =  new UserInfo("alice","password1",params);
      UserInfo bob =  new UserInfo("bob","password1",params);

      JPAKEAlgorithm alicePake = new JPAKEAlgorithm(alice);
      JPAKEAlgorithm bobPake = new JPAKEAlgorithm(bob);

      /* Fase 1
       * Genero primo payload per alice e bob
       */
      PayloadPhase1 pahse1Alice = alicePake.createPayloadPhase1();
      PayloadPhase1 pahse1Bob = bobPake.createPayloadPhase1();
      System.out.println("Fase 1 Valida.");

      /**
       * Fase 2
       * Scambio dei payload tra bob ed alice
       */
      PayloadPhase2 pahse2Alice = alicePake.createPayloadPhase2(pahse1Bob);
      PayloadPhase2 pahse2Bob   = bobPake.createPayloadPhase2(pahse1Alice);
      System.out.println("Fase 2 Valida.");

      /**
       * Fase 3
       * Genero chiave
       */
      BigInteger aliceKey = alicePake.generateKeyMaterial(pahse1Bob,pahse2Bob);
      BigInteger bobKey = bobPake.generateKeyMaterial(pahse1Alice,pahse2Alice);
      System.out.println("Fase 3 Valida.");


      System.out.println("Alice key \t K="+aliceKey.toString(16));
      System.out.println("Bob key \t K="+bobKey.toString(16));


    } catch (Exception e) {
      System.out.print( e.getMessage());
    }

  }
}
