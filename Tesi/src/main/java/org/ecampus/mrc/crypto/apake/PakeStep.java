package org.ecampus.mrc.crypto.apake;

public enum PakeStep {
  CLIENT_VU, SERVER_Y, SERVER_VS, CLIENT_X,REGISTER,COMPLETE
}
