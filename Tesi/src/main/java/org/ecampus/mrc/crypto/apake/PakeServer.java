package org.ecampus.mrc.crypto.apake;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.ecampus.mrc.crypto.utils.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PakeServer {

  private static final Logger LOG = LoggerFactory.getLogger(PakeServer.class);

  private final DSAGroupParams groupParams;
  private final String serverID;
  private String clientID;
  private BigInteger passwordVerified;
  private MessageDigest md;

  private BigInteger y;
  private BigInteger Y;
  private BigInteger X;
  private byte [] SK;

  public PakeServer(DSAGroupParams groupParams, String serverID) throws NoSuchAlgorithmException {
    this(groupParams, serverID, MessageDigest.getInstance("SHA-256"));
  }

  public PakeServer(DSAGroupParams groupParams, String serverID, MessageDigest md) {
    LOG.info("INIT");
    this.groupParams = groupParams;
    this.md = md;
    this.serverID = serverID;
  }

  public void registration(APakePayload payload) {
    LOG.info("Register Client " + payload.getId());
    this.clientID = payload.getId();
    this.passwordVerified = payload.getValue();
  }

  /**
   * Fase 2 S-->U: (S,Y)
   *
   * y = random from Z_q^+ r = H(0x01!|U|S|X) Y = (X * (W^r))^y mod p
   */
  public APakePayload generateY(APakePayload in) throws IllegalArgumentException {
    LOG.info("Received X");
    if (!in.getValue().equals(clientID)) {
      new IllegalArgumentException("Invalid client ID");
    }

    this.X = in.getValue();
    if (X.mod(groupParams.getP()).equals(NumberUtils.ZERO) ||
        X.mod(groupParams.getP()).equals(NumberUtils.ONE) ||
        X.mod(groupParams.getP()).equals(NumberUtils.MINUS_ONE)) {
      throw new IllegalArgumentException("Invalid X");
    }

    y = NumberUtils.generateRandomNumberModuleQ(groupParams.getQ(), new SecureRandom());
    List<BigInteger> val = Arrays.asList(new BigInteger(CryptoUtils.char1),
        new BigInteger(clientID.getBytes()), new BigInteger(serverID.getBytes()),
        X);

    Y = generateY(val);

    LOG.info("Generated Y");

    return new APakePayload(clientID, Y, new Date(), PakeStep.SERVER_Y);
  }

  /**
   * Fase 4 BigInteger V_U
   *
   * K = g^y mod p V_U(server) = = H(0x02|U|S|X|Y|K)
   *
   * V_S = H(0x03|U|S|X|Y|K) SK = H(0x04|U|S|X|Y|K)
   */
  public APakePayload generateVSandSK(APakePayload in) throws IllegalArgumentException {

    LOG.info("Received V_U");
    if (!in.getId().equals(clientID)) {
      throw new IllegalArgumentException("Invalid client ID");
    }

    BigInteger K = groupParams.getG().modPow(y, groupParams.getP());

    List<BigInteger> val = Arrays
        .asList(new BigInteger((CryptoUtils.char2)), new BigInteger(clientID.getBytes()),
            new BigInteger(serverID.getBytes()), X, Y, K);

    /*System.out.println("################ SERVER ################\n"
        + "1 - "+ new BigInteger((CryptoUtils.char2)) +"\n"
        + "2 CLIENT ID - "+ new BigInteger(clientID.getBytes()) +"\n"
        + "3 - SERVER ID - "+ new BigInteger(serverID.getBytes()) +"\n"
        + "4 X - "+ X +"\n"
        + "5 Y - "+ Y +"\n"
        + "5 K - "+ K +"\n"
    );
    */

    BigInteger V_U_Server = new BigInteger(CryptoUtils.getHashBigInteger(md, val));

    if (!V_U_Server.equals(in.getValue())) {
      throw new IllegalArgumentException("Invalid V_U");
    }

    val = Arrays.asList(new BigInteger((CryptoUtils.char3)), new BigInteger(clientID.getBytes()),
        new BigInteger(serverID.getBytes()), X, Y, K);
    BigInteger V_S = new BigInteger(CryptoUtils.getHashBigInteger(md, val));

    LOG.info("Generate V_S");

    // Session Key for server
    val = Arrays.asList(new BigInteger((CryptoUtils.char4)), new BigInteger(clientID.getBytes()),
        new BigInteger(serverID.getBytes()), X, Y, K);
    SK = CryptoUtils.getHashBigInteger(md,val);
    //SK = new String (Hex.encodeHex(CryptoUtils.getHash(md,val)));
    resetParameters();

    LOG.info("Session Key generated");

    return new APakePayload(clientID, V_S, new Date(), PakeStep.SERVER_VS);
  }

  private void resetParameters() {
    LOG.info("Reset private Parameters");
    y = null;
    X = null;
    Y = null;
  }

  private BigInteger generateY(List<BigInteger> val) {
    BigInteger r = new BigInteger(CryptoUtils.getHashBigInteger(md, val));
    BigInteger X1 = X.mod(groupParams.getP());
    BigInteger wr = passwordVerified.modPow(r, groupParams.getP());
    Y = X1.multiply(wr).modPow(y, groupParams.getP());

    return Y;
  }

  public BigInteger getPasswordVerified() {
    return passwordVerified;
  }

  public byte[] getSessionKey() {
    return SK;
  }
}
