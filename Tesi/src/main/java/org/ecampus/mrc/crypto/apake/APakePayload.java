package org.ecampus.mrc.crypto.apake;

import java.math.BigInteger;
import java.util.Date;

public class APakePayload {

  private String id;
  private BigInteger value;
  private Date generatedTime;
  private PakeStep step;

  public APakePayload() {

  }

  public APakePayload(String id, BigInteger value, Date generatedTime, PakeStep step) {
    this.id = id;
    this.value = value;
    this.generatedTime = generatedTime;
    this.step = step;
  }

  public PakeStep getStep() {
    return step;
  }

  public Date getGeneratedTime() {
    return generatedTime;
  }

  public String getId() {
    return id;
  }

  public BigInteger getValue() {
    return value;
  }

}
