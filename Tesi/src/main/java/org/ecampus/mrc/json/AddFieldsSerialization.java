package org.ecampus.mrc.json;


import java.io.IOException;
import java.util.Map;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.ser.BeanSerializerModifier;
import org.codehaus.jackson.map.ser.std.BeanSerializerBase;

public class AddFieldsSerialization extends BeanSerializerModifier {

  Map<String, String> input;
  public AddFieldsSerialization (Map<String, String> input){
    this.input = input;
  }

  @Override
  public JsonSerializer<?> modifySerializer(SerializationConfig config,
      BasicBeanDescription beanDesc, JsonSerializer<?> serializer) {
    if (serializer instanceof BeanSerializerBase) {
      return new ExtraFieldSerializer((BeanSerializerBase) serializer,input);
    }
    return serializer;
  }


  private class ExtraFieldSerializer extends BeanSerializerBase {

    private final Map<String, String> input;

    ExtraFieldSerializer(BeanSerializerBase source,Map<String,String> input) {
      super(source);
      this.input = input;
    }
     public void serialize(Object bean, JsonGenerator jgen,
        SerializerProvider provider) throws IOException,
        JsonGenerationException {
      jgen.writeStartObject();
      serializeFields(bean, jgen, provider);
      input.forEach((k,v)->{
        try {
          jgen.writeStringField(k, v);
        } catch (IOException e) {
          e.printStackTrace();
        }

      });

      jgen.writeEndObject();
    }
  }

}
