package org.ecampus.mrc.json;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.SerializerFactory;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.ser.BeanSerializerFactory;
import org.codehaus.jackson.node.ObjectNode;

public class JSONUtils {

  public static final String DEFAULT_TYPE_KEY = "typeCls";

  public static String toJson(Object obj, Map<String, String> in) throws IOException {
    ObjectMapper mapper = new ObjectMapper();

    mapper.configure(Feature.INDENT_OUTPUT, true);
    mapper.configure(Feature.SORT_PROPERTIES_ALPHABETICALLY, true);

    SerializerFactory serializerFactory = BeanSerializerFactory.instance
        .withSerializerModifier(new AddFieldsSerialization(in));
    mapper.setSerializerFactory(serializerFactory);

    SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
    mapper.setDateFormat(outputFormat);
    mapper.setSerializationInclusion(Inclusion.NON_DEFAULT.NON_EMPTY);

    try (StringWriter writer = new StringWriter()) {
      mapper.writeValue(writer, obj);
      writer.flush();
      byte[] base64 = Base64.getEncoder().encode(writer.toString().getBytes());
      return new String(base64);
    }
  }

  public static String toJson(Object obj) throws IOException {
    Map<String, String> maps = new HashMap<>();
    maps.put(DEFAULT_TYPE_KEY, obj.getClass().getName());
    return toJson(obj, maps);
  }

  public static <T extends Object> T fromJson(String json) throws IOException {
    byte[] fromBase64 = Base64.getDecoder().decode(json);

    ObjectMapper mapper = new ObjectMapper();
    ObjectNode node = new ObjectMapper().readValue(new String(fromBase64), ObjectNode.class);
    try {
      if (node.has(DEFAULT_TYPE_KEY)) {
        String classType = node.get(DEFAULT_TYPE_KEY).asText();

        Class clazz = Class.forName(classType);
        return fromJson(json, (Class<T>) clazz);

      }
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static <T extends Object> T fromJson(String json, Class<T> clazz) throws IOException {
    byte[] fromBase64 = Base64.getDecoder().decode(json);
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper.readValue(new String(fromBase64), clazz);
  }
}
