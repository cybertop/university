package org.ecampus.mrc.mqtt;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.apake.PakeServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InMemoryPakeSteps {

  private static final Logger LOG = LoggerFactory.getLogger(InMemoryPakeSteps.class);
  private static final String PROPERTY_FILE_NAME = "DSAParams.properties";

  private Map<String, PakeServer> inMemoryMap;
  private static InMemoryPakeSteps memoryPakeSteps;
  private DSAGroupParams groupParams;

  static {
    try {
      memoryPakeSteps = new InMemoryPakeSteps();
    } catch (IOException e) {
      LOG.error("Error during initialize --> " + e.getMessage());
    }
  }

  private InMemoryPakeSteps() throws IOException {
    LOG.info("Intit in memory Step");
    inMemoryMap = new HashMap<String, PakeServer>();
    Properties properties = new Properties();
    InputStream in = getClass().getResourceAsStream("/"+PROPERTY_FILE_NAME);
    if(in == null ) in = ClassLoader.getSystemResourceAsStream(PROPERTY_FILE_NAME);
    properties.load(in);
    groupParams = new DSAGroupParams(properties);
  }

  public static InMemoryPakeSteps getInstance() {
    return memoryPakeSteps;
  }


  public PakeServer getPakeServer(String clientId, String serverId) throws NoSuchAlgorithmException {
    PakeServer pakeServer = inMemoryMap.get(clientId);
    if (pakeServer == null) {
      pakeServer = new PakeServer(groupParams, serverId);
      inMemoryMap.put(clientId, pakeServer);
      LOG.info("Add new Pake Server for " + clientId);

    }
    return pakeServer;
  }

  public void removePakeServer(String client) {
    inMemoryMap.remove(client);
    LOG.info("Remore Pake Server for " + client);
  }
}
