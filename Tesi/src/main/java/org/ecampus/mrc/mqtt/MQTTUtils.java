package org.ecampus.mrc.mqtt;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang3.ArrayUtils;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;

public class MQTTUtils {

  private final static String TOPIC_SUFFIX = "tmp/auth";
  private final static String TOKEN_SEPARATOR = "\\|";

  /**
   *
   * @param host
   * @param port
   * @param destination
   * @param clientId
   * @param user
   * @param password
   * @param message
   * @param qoS
   * @throws MqttException
   */
  public static void sendMessageTopic(String host, int port, String destination, String clientId,
      String user, String password, byte[] message,QoS qoS) throws MqttException {
    BlockingConnection connection = null;
    MQTT mqtt;
    try {
      mqtt = new MQTT();
      mqtt.setHost(host, port);
      mqtt.setClientId(clientId);
      mqtt.setUserName(user);
      mqtt.setPassword(password);
      connection = mqtt.blockingConnection();
      System.out.println("sendMessageTopic CONNECT"+ new SimpleDateFormat("dd-MM-yy:HH:mm:SS").format(new Date()) +" Message size in kb: "+message.length/1024);
      connection.connect();
      connection.publish(destination, message, qoS, false);

    } catch (Exception e) {
      throw new MqttException(e);
    } finally {
      if (connection != null && connection.isConnected()) {
        try {
          connection.disconnect();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   *
   * @param host
   * @param port
   * @param destination
   * @param clientId
   * @param message
   * @param qoS
   * @throws MqttException
   */
  public static void sendMessageTopic(String host, int port, String destination, String clientId,
      byte[] message, QoS qoS)
      throws MqttException {
    sendMessageTopic(host, port, destination, clientId, null, null, message,qoS);
  }

  /**
   *
   * @param clientId
   * @param passwordVerify
   * @return
   */
  public static String generateAuthReponseTopic(String clientId, BigInteger passwordVerify) {
    StringBuffer buffer = new StringBuffer();
    buffer.append(TOPIC_SUFFIX);
    buffer.append("/");
    buffer.append(clientId);
    buffer.append("_");
    buffer.append(passwordVerify.toString().substring(0,5));
    return buffer.toString();
  }

  /**
   *
   * @return
   */
  public static Pattern getRegexMetchTopic(){
    String topic [] = TOPIC_SUFFIX.split("/");
    StringBuffer regex = new StringBuffer();

    Arrays.asList(topic).stream().forEach(s->regex.append(s+"[.|\\/]"));
    return Pattern.compile(regex.toString());
  }

  /**
   *
   * @param topic
   * @return
   */
  public static String decodeTopicName(String topic){
    return topic.replaceAll("\\/", ".");
  }

  /**
   *
   * @param auth
   * @return
   */
  public static String getTopicFromAuthToken(String auth){
    //TODO DA sistemare
    String topic = auth.split(TOKEN_SEPARATOR)[0];
    return topic;
  }/**/

  /**
   *
   * @param authToken
   * @param key
   * @param optional
   * @return
   * @throws Exception
   */
  public static boolean validateToken(String authToken,byte[] key,String... optional)throws Exception{
    String values [] = authToken.split(TOKEN_SEPARATOR);
    String valuesNoHash [] = Arrays.copyOfRange(values,0,values.length - 1);
    List<String> valueList  = Arrays.asList(ArrayUtils.addAll(valuesNoHash, optional));
    byte[] macSource = CryptoUtils.decodeBase64(values[values.length-1].getBytes());
    return CryptoUtils.verifyMAC(key,valueList,macSource);
  }

  /**
   *
   * @param input
   * @param start
   * @param length
   * @return
   */
  public static byte[] extractContent(byte[] input,int start, int length){

    if(start < 0 ) start = 0;
    byte [] correctData = new byte[length-start];
    if(input != null && input.length >= length)
      System.arraycopy(input, start, correctData, 0, length-start);

    return correctData;
  }

}
