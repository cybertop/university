package org.ecampus.mrc.mqtt.subscribe;

import org.apache.commons.lang3.StringUtils;
import org.ecampus.mrc.activemq.AMQBroker;
import org.ecampus.mrc.json.JSONUtils;
import org.ecampus.mrc.model.TopicSession;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.ecampus.mrc.repository.TopicSessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;

public class CreateTopicSubscriber {

  private final Logger LOG = LoggerFactory.getLogger(getClass());

  @Autowired
  private TopicSessionRepository topicSessionRepo;

  public void handleMessage(Message<?> message) throws MessagingException {
    LOG.info("Received message " + message.getHeaders().getId());
    LOG.debug("Message Header " + message.getHeaders().toString());

    if (message.getPayload() == null) {
      throw new MessagingException("Paylod not valid ");
    }

    try {
      TopicSession payload;
      if (message.getPayload() instanceof String) {
        payload = JSONUtils.fromJson(message.getPayload().toString());
      } else if (message.getPayload().getClass().isInstance(TopicSession.class)) {
        payload = ((TopicSession) message.getPayload());
      } else {
        throw new MessagingException("Payload not accepted");
      }

      if(StringUtils.isNotEmpty(payload.getTopicName())) {
        topicSessionRepo.save(payload);
        String topic = MQTTUtils.getTopicFromAuthToken(payload.getTopicName());
        LOG.debug("Add new topic " + topic);
        AMQBroker.getDestinationFacade().createDestination(topic);
      }
    }catch (Exception e) {
      LOG.error(e.getMessage(),e);
    }

  }
}
