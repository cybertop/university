/**
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The ASF licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License.  You may obtain a
 * copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package org.ecampus.mrc.mqtt;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.CallbackConnection;
import org.fusesource.mqtt.client.Listener;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubscribeTopic implements Runnable {

  private final Logger LOG = LoggerFactory.getLogger(getClass());

  private final String host;
  private final int port;
  private final String clientId;
  private final ArrayList<Topic> topics = new ArrayList<Topic>();
  private final Listener listener;

  private MQTT mqtt;
  private CallbackConnection connection;
  private ExecutorService service;


  public SubscribeTopic(String host, int port, String clientId, String topic, Listener listener) {
    LOG.info("Initialize Subscribe");
    topics.add(new Topic(topic, QoS.AT_MOST_ONCE));
    this.listener = listener;
    this.host = host;
    this.port = port;
    this.clientId = clientId;
  }

  public void execute() {

    service = Executors.newSingleThreadExecutor();
    service.submit(this);
  }

  public void stop() {
    connection.disconnect(new Callback<Void>() {
      @Override
      public void onSuccess(Void value) {

      }

      @Override
      public void onFailure(Throwable value) {

      }
    });
    service.shutdown();
  }

  @Override
  public void run() {
    LOG.info(getClass().getName() + " Start in Async Mode");
    try {
      mqtt = new MQTT();
      mqtt.setHost(host, port);
      mqtt.setClientId(clientId);
      connection = mqtt.callbackConnection();

      connection.resume();
      connection.connect(new InternalCallback());
    } catch (URISyntaxException e) {
      LOG.error(e.getMessage(), e);
    }

  }

  private class InternalCallback implements Callback<Void> {

    @Override
    public void onSuccess(Void value) {
      final Topic[] ta = topics.toArray(new Topic[topics.size()]);
      connection.listener(listener);
      connection.subscribe(ta, new Callback<byte[]>() {
        public void onSuccess(byte[] value) {

          for (int i = 0; i < value.length; i++) {
            LOG.info(
                "Subscribed to Topic: " + ta[i].name() + " with QoS: " + QoS.values()[value[i]]);
          }
        }

        @Override
        public void onFailure(Throwable value) {
          LOG.error(value.getMessage());
        }
      });
    }

    @Override
    public void onFailure(Throwable value) {
      LOG.error(value.getMessage());
    }
  }

}