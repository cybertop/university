package org.ecampus.mrc.mqtt.subscribe;


import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.lang3.StringUtils;
import org.ecampus.mrc.activemq.AMQBroker;
import org.ecampus.mrc.crypto.apake.APakePayload;
import org.ecampus.mrc.crypto.apake.PakeServer;
import org.ecampus.mrc.json.JSONUtils;
import org.ecampus.mrc.model.ClientSession;
import org.ecampus.mrc.mqtt.InMemoryPakeSteps;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.ecampus.mrc.repository.ClientRegistrationRepository;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;

public class RegistrationSubscriber {

  @Autowired
  private ClientRegistrationRepository repository;

  //TODO rendere generico l'accesso alla configurazione del Broker

  private static final Logger LOG = LoggerFactory.getLogger(RegistrationSubscriber.class);
  private final InMemoryPakeSteps memoryPakeSteps = InMemoryPakeSteps.getInstance();

  public void handleMessage(Message<?> message) throws MessagingException {
    LOG.info("Received message " + message.getHeaders().getId());
    LOG.debug("Message Header " + message.getHeaders().toString());

    if (message.getPayload() == null) {
      throw new MessagingException("Paylod not valid ");
    }

    try {
      APakePayload payload;
      if (message.getPayload() instanceof String) {
        payload = JSONUtils.fromJson(message.getPayload().toString());
      } else if (message.getPayload().getClass().isInstance(APakePayload.class)) {
        payload = ((APakePayload) message.getPayload());
      } else {
        throw new MessagingException("Payload not accepted");
      }

      algotihmSteps(payload);

      LOG.info("Client " + payload.getId());

    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
    }
  }

  private void algotihmSteps(APakePayload payload)
      throws Exception {

    PakeServer pakeServer = memoryPakeSteps
        .getPakeServer(payload.getId(), AMQBroker.getInstance().getBrokerName());
    String responseTopicName = null;
    APakePayload response = null;
    try {
      switch (payload.getStep()) {
        case REGISTER:
          if(repository.findOne(payload.getId()) != null)
            repository.delete(payload.getId());
          pakeServer.registration(payload);
          break;
        case CLIENT_X:
          response = pakeServer.generateY(payload);
          responseTopicName = MQTTUtils
              .generateAuthReponseTopic(payload.getId(), pakeServer.getPasswordVerified());
          sendeRespnse(responseTopicName, response, payload.getId());
          break;
        case CLIENT_VU:
          response = pakeServer.generateVSandSK(payload);
          responseTopicName = MQTTUtils
              .generateAuthReponseTopic(payload.getId(), pakeServer.getPasswordVerified());
          sendeRespnse(responseTopicName, response, payload.getId());
          break;
        case COMPLETE:
          responseTopicName = MQTTUtils
              .generateAuthReponseTopic(payload.getId(), pakeServer.getPasswordVerified());
          AMQBroker.getDestinationFacade().removeTopic(responseTopicName);
          memoryPakeSteps.removePakeServer(payload.getId());

          byte[] sk = pakeServer.getSessionKey();
          saveSessionKey(payload, sk);

          break;
      }
    } catch (Exception e) {
      if (pakeServer != null) {
        responseTopicName = MQTTUtils
            .generateAuthReponseTopic(payload.getId(), pakeServer.getPasswordVerified());
        if (StringUtils.isNotEmpty(responseTopicName)) {
          AMQBroker.getDestinationFacade().removeTopic(responseTopicName);
        }
      }
      throw e;

    }
  }

  private void sendeRespnse(String responseTopicName, APakePayload payload, String clientId)
      throws IOException, MqttException {
    String message = JSONUtils.toJson(payload);
    MQTTUtils
        .sendMessageTopic(AMQBroker.getInstance().getHost(), AMQBroker.getInstance().getPort(),
            responseTopicName, clientId, message.getBytes(),
            QoS.AT_LEAST_ONCE);
  }

  private void saveSessionKey(APakePayload payload, byte[] sk) throws NoSuchAlgorithmException {

    ClientSession registration = new ClientSession();
    registration.setId(payload.getId());
    registration.setRegistrationDate(payload.getGeneratedTime());
    registration.setSessionKey(sk);
    registration.setValid(true);

    repository.saveAndFlush(registration);
  }
}
