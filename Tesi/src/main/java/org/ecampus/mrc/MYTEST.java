package org.ecampus.mrc;

import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.ecampus.mrc.mqtt.MQTTUtils;

public class MYTEST {

  public static void main (String args []) {
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");
      String start = "20-09-18:14:53:311";
      String end = "20-09-18:14:53:348";
      System.out.println(Utils.getDateDiff(dateFormat.parse(start),dateFormat.parse(end), TimeUnit.MILLISECONDS));
    }catch (Exception e){

    }
  }
}
