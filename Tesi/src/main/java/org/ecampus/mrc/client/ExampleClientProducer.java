package org.ecampus.mrc.client;

import java.util.Properties;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.symmetric.AES;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.fusesource.mqtt.client.QoS;

public class ExampleClientProducer {

  private static final String DESTINATION = "crypto/registration";
  private static final String HOST = "127.0.0.1";
  private static final int PORT = 1883;
  private static final String PASSWOD = "password";
  private static final String CLIENT_ID = "MY_DEVICE";
  private static final String SERVER_ID = "amq-broker";
  private static final String PROPERTY_FILE_NAME = "DSAParams.properties";

  private static final String TOPIC_CODE = "AAABBC6";
  private static final String DESTINATION_CREATE_TOPIC = "crypto/createTopic";

  public static void main(String args[]) {
    Properties properties = new Properties();
    try {
      properties.load(ClassLoader.getSystemResourceAsStream(PROPERTY_FILE_NAME));
      DSAGroupParams groupParams = new DSAGroupParams(properties);
      ExchangeKeyRequest request = new ExchangeKeyRequest(DESTINATION, HOST, PORT, PASSWOD,
          CLIENT_ID, SERVER_ID, groupParams);

      request.generateSessionKey(new ExchangeKeyCallback() {

        @Override
        public void onReceviceKey(byte[] key) {
          try {
            System.out.println("key prducer = [" + new String(key) + "]");

            CreateTopicRequest createTopicRequest = new CreateTopicRequest(DESTINATION_CREATE_TOPIC,
                HOST, PORT,
                CLIENT_ID, TOPIC_CODE, "my/topicName", key);
            createTopicRequest.sendMessage();
            System.out.println("Topic code " + createTopicRequest.getTopicCode());

            //Send message to topic
            int i = 0;
            AES aes = new AES(key);
            byte[] mess = null;
            while (i < 100) {
              i++;
              mess = aes.encrypt(createDataSize(2).getBytes());
              MQTTUtils.sendMessageTopic(HOST, PORT, "my/topicName", CLIENT_ID,
                  mess,
                  QoS.AT_LEAST_ONCE);
              System.out.println("Send message " + new String(aes.decrypt(mess)));
              Thread.sleep(50000);
            }


          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });

    } catch (Exception e) {
      e.printStackTrace();
    }

  }


  /**
   * Creates a message of size @msgSize in KB.
   */
  private static String createDataSize(int msgSize) {
    // Java chars are 2 bytes
    //msgSize = msgSize/2;
    msgSize = msgSize * 1024;
    StringBuilder sb = new StringBuilder(msgSize);
    for (int i=0; i<msgSize; i++) {
      sb.append('a');
    }
    return sb.toString();
  }
}
