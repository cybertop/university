package org.ecampus.mrc.client;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.ecampus.mrc.Utils;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.apake.APakePayload;
import org.ecampus.mrc.crypto.apake.PakeClient;
import org.ecampus.mrc.crypto.apake.PakeStep;
import org.ecampus.mrc.json.JSONUtils;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.ecampus.mrc.mqtt.SubscribeTopic;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.Listener;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExchangeKeyRequest implements Listener {

  private final Logger LOG = LoggerFactory.getLogger(getClass());

  private String destination;
  private String host;
  protected String clientId;
  private int port;
  protected PakeClient pakeClient;
  protected ExchangeKeyCallback callback;
  private SubscribeTopic subscribeTopic;
  private Date start;
  private Date end;
  public ExchangeKeyRequest(String destination, String host, int port, String password,
      String clientId, String serverID,
      DSAGroupParams groupParams) throws NoSuchAlgorithmException {
    this.destination = destination;
    this.host = host;
    this.port = port;
    this.clientId = clientId;
    pakeClient = new PakeClient(groupParams, password, clientId, serverID);
  }

  public void generateSessionKey(ExchangeKeyCallback callback) throws Exception {
    this.callback = callback;
    start = new Date();
    System.out.println("Start Generate KEY "+ new SimpleDateFormat("dd-MM-yy:HH:mm:SS").format(start));
    APakePayload pakePayload = pakeClient.generateRegistrationPaylod();
    //Invio password verify
    String message = JSONUtils.toJson(pakePayload);
    BigInteger passwordVerify = pakePayload.getValue();
    MQTTUtils
        .sendMessageTopic(host, port, destination, clientId, message.getBytes(),
            QoS.AT_LEAST_ONCE);

    initializeSubscribe(passwordVerify);

    //Genero ed invio al server X
    APakePayload X = pakeClient.generateX();
    message = JSONUtils.toJson(X);
    MQTTUtils
        .sendMessageTopic(host, port, destination, clientId, message.getBytes(),
            QoS.AT_LEAST_ONCE);

  }

  private void initializeSubscribe(BigInteger passwordVerify) throws Exception {
    String topic = MQTTUtils.generateAuthReponseTopic(clientId, passwordVerify);
    subscribeTopic = new SubscribeTopic(host, port, clientId, topic, this);
    subscribeTopic.execute();
  }

  /************************* Listener Method ***************/
  @Override
  public void onConnected() {
   // LOG.info("Client Subscribe connect for Exchange Key Rquest");
  }

  @Override
  public void onDisconnected() {
   // LOG.info("Client Subscribe disconnected from Exchange Key Rquest");

  }

  @Override
  public void onPublish(UTF8Buffer topi, Buffer body, Runnable ack) {

    try {
      APakePayload pakePayload = JSONUtils
          .fromJson(new String(body.toByteArray()), APakePayload.class);

      if (pakePayload.getStep() == PakeStep.SERVER_Y) {
        LOG.info("Client receive Y from server");
        APakePayload V_U = pakeClient.generateVU(pakePayload);
        //Send new Message
        String message = JSONUtils.toJson(V_U);
        MQTTUtils
            .sendMessageTopic(host, port, destination, clientId, message.getBytes(),
                QoS.AT_LEAST_ONCE);

      } else if (pakePayload.getStep() == PakeStep.SERVER_VS) {
        LOG.info("Client receive VS from server");

        pakeClient.computeSessionKey(pakePayload);

        byte[] sk_client = pakeClient.getSessionKey();
        APakePayload complete = new APakePayload(clientId, null, null, PakeStep.COMPLETE);
        String message = JSONUtils.toJson(complete);
        MQTTUtils.sendMessageTopic(host, port, destination, clientId, message.getBytes(),
            QoS.AT_LEAST_ONCE);
        //call callback method to send key
        if (callback != null) {
          end = new Date();
          System.out.println("END Generate KEY "+ new SimpleDateFormat("dd-MM-yy:HH:mm:SS").format(end));
          System.out.println("Time to generate key millisecond "+ Utils.getDateDiff(start,end,
              TimeUnit.MILLISECONDS));
          callback.onReceviceKey(sk_client);
          subscribeTopic.stop();
        }
      }

    } catch (Exception e) {
      onFailure(e);
    }
  }

  @Override
  public void onFailure(Throwable value) {
    LOG.error("Listener failure " + value.getMessage());
    subscribeTopic.stop();
  }
}
