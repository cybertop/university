package org.ecampus.mrc.client;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.ecampus.mrc.crypto.DSAGroupParams;
import org.ecampus.mrc.crypto.symmetric.AES;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.ecampus.mrc.mqtt.SubscribeTopic;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.Listener;

public class ExampleClientSubscribe implements Listener {

  private static final String DESTINATION = "crypto/registration";
  private static final String HOST = "127.0.0.1";
  private static final int PORT = 1883;
  private static final String PASSWOD = "password";
  private static final String CLIENT_ID = "Subscriber_1";
  private static final String SERVER_ID = "amq-broker";
  private static final String PROPERTY_FILE_NAME = "DSAParams.properties";

  private static final String TOKEN_TOPIC = "AAABBC6";

  private byte[] key;

  public static void main(String args[]) {
    ExampleClientSubscribe listener = new ExampleClientSubscribe();
    Properties properties = new Properties();
    try {
      properties.load(ClassLoader.getSystemResourceAsStream(PROPERTY_FILE_NAME));
      DSAGroupParams groupParams = new DSAGroupParams(properties);
      ExchangeKeyRequest request = new ExchangeKeyRequest(DESTINATION, HOST, PORT, PASSWOD,
          CLIENT_ID, SERVER_ID, groupParams);

      request.generateSessionKey(new ExchangeKeyCallback() {
        @Override
        public void onReceviceKey(byte[] key) {
          listener.key = key;
          System.out.println("key producer = [" + key + "]");
          try {
            AES aes = new AES(key);

            //GENERA IL AUTH2
            List<String> values = Arrays.asList(TOKEN_TOPIC,"32",CLIENT_ID);
            String auth2 = TOKEN_TOPIC+"|"+"32|"+new String(CryptoUtils.encodeBase64(CryptoUtils.getMAC(key,values)));

            String topic = new String(CryptoUtils.encodeBase64(aes.encrypt(auth2.getBytes())));
            System.out.println("########################################## TOPIC SENDED: "+topic);
            SubscribeTopic subscribeTopic = new SubscribeTopic(HOST, PORT, CLIENT_ID, topic,listener);
            subscribeTopic.execute();

          } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
          }
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(2);
    }
  }

  @Override
  public void onConnected() {
    System.out.println("Connesso");
  }

  @Override
  public void onDisconnected() {
    System.out.println("onDisconnected");
  }

  @Override
  public void onPublish(UTF8Buffer topic, Buffer body, Runnable ack) {
    try {
      AES aes = new AES(key);
      if (body != null) {
        byte[] fixedContect = MQTTUtils.extractContent(body.getData(),body.offset,body.getData().length);
        String message = new String(aes.decrypt(fixedContect));
        System.out.println("Receive Message "+ new SimpleDateFormat("dd-MM-yy:HH:mm:SS").format(new Date()) +" Message size: "+message.getBytes().length/1024);
        ack.run();
      }
    } catch (Exception e) {
      onFailure(e);
    }
  }

  @Override
  public void onFailure(Throwable value) {
    value.printStackTrace();
    System.exit(2);
  }
}
