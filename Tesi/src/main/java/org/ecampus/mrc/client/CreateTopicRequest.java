package org.ecampus.mrc.client;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.lang3.RandomStringUtils;
import org.ecampus.mrc.crypto.symmetric.AES;
import org.ecampus.mrc.crypto.symmetric.SymmetricAlgorithm;
import org.ecampus.mrc.crypto.utils.CryptoUtils;
import org.ecampus.mrc.json.JSONUtils;
import org.ecampus.mrc.model.TopicSession;
import org.ecampus.mrc.model.TopicSessionMessage;
import org.ecampus.mrc.mqtt.MQTTUtils;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.fusesource.mqtt.client.QoS;

public class CreateTopicRequest {

  private final TopicSession topicSession;
  private final String destination;
  private final String host;
  private final int port;
  private final byte[] key;


  public CreateTopicRequest(String destination, String host, int port,
      String clientId, String topicCode, String topicName, byte[] key)
      throws NoSuchAlgorithmException,InvalidKeyException {

    //TODO Verificare se usare il topiccode (token) o il topic name
    String bs = new String(CryptoUtils.encodeBase64(CryptoUtils.getMAC(key,Arrays.asList(topicName))));
    String auth1 = topicName+"|"+ bs;
    System.out.println("##### AUTH!: "+auth1);
    topicSession = new TopicSession(topicCode, clientId, auth1);
    this.destination = destination;
    this.host = host;
    this.port = port;
    this.key = key;
  }

  public CreateTopicRequest(String destination, String host, int port,
      String clientId, String topicName, byte[] key) throws NoSuchAlgorithmException,InvalidKeyException {
    this(destination, host, port, clientId, RandomStringUtils.randomAlphanumeric(6), topicName,
        key);
  }
//TODO SHOW PROF La richiesta di creazione di un topic è un messaggio normale con il payload cifrato AUTH1 potrebbe essere non cnecessario
  public void sendMessage()
      throws IOException, MqttException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

    SymmetricAlgorithm symmetricProducer = new AES(key);
    String message = JSONUtils.toJson(topicSession);

    byte[] encryptedMesssage = symmetricProducer.encrypt(message.getBytes());

    TopicSessionMessage  topicSessionMessage = new TopicSessionMessage(topicSession.getClientId(),encryptedMesssage);
    message = JSONUtils.toJson(topicSessionMessage);
    //System.out.println(message);
    MQTTUtils
        .sendMessageTopic(host, port, destination, topicSession.getClientId(), message.getBytes(),
            QoS.AT_LEAST_ONCE);

  }

  public String getTopicCode() {
    return topicSession.getId();
  }
}
